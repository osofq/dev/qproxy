#!/usr/bin/env node

const mri = require('mri')
const { qproxy } = require('./proxy')

// console.log(process.env)

const { _, ...opts } = mri(process.argv.slice(2))

const { p = 0, d = null } = opts

let valid = true
if (p === 0) {
    valid = false
    console.log('Invalid argument - port (-p)')
}
if (d === null) {
    valid = false
    console.log('Invalid argument - domain(s) (-d)')
}

if (valid) {
    qproxy({
        p,
        d,
        env: {
            HOME: process.env.HOME,
            PROJECT_DIRECTORY: __dirname
        }
    })
} else {
    console.log('\nUsage: qproxy -p 3000 -d example.com')
    console.log('       qproxy -p 3000 -d example.com,www.example.com\n')
}
