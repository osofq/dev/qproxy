const package = require('../package.json')
const fs = require('fs')
const micro = require('micro')
const { send, json } = require('micro')
const fetch = require('isomorphic-unfetch')
const openssl = require('openssl-nodejs')
const { execSync } = require('child_process')

console.log(`${package.description} - v${package.version}\n`)
console.log('**************************************************************\n')

// Global variables
let redbird
let homePath, qproxyConfigPath, qproxyCertPath

// Helper functions
const createDirectoryIfNotExists = path => {
    try {
        const stats = fs.lstatSync(path)
    } catch (err) {
        fs.mkdirSync(path, { recursive: true })
    }
}

// Utils

// Check if Q Proxy config direcotry exists in the home directory of the current user, if not, create it
const setup = async () => {
    createDirectoryIfNotExists(qproxyConfigPath)
    createDirectoryIfNotExists(qproxyCertPath)
}

// Generate config file needed by OpenSSL for generating SSL Certificate
const generateConfig = async domains => {
    const commonName = domains[0]

    if (fs.existsSync(`${qproxyCertPath}/${commonName}.crt`)) {
        return
    }

    console.log(`Creating for config for SSL Certificate - ${commonName} ...`)
    const altNames = domains.map((domain, i) => `DNS.${i} = "${domain}"`).join('\n')

    const config = `[ req ]
default_bits       = 4096
default_md         = sha512
default_keyfile    = server.key
prompt             = no
encrypt_key        = no
distinguished_name = req_distinguished_name
x509_extensions    = v3_req

# distinguished_name
[ req_distinguished_name ]
countryName            = "US"                                        # C=
stateOrProvinceName    = "California"                                # S=
localityName           = "San Francisco"                             # L=
organizationName       = "Solar Inc."                                # O=
organizationalUnitName = "Earth"                                     # OU=
commonName             = "${commonName}"                               # CN=
emailAddress           = "human@earth.org"                           # CN/emailAddress=

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[ alt_names ]
${altNames}
`

    fs.writeFileSync(`${qproxyCertPath}/${commonName}.cnf`, config, { encoding: 'utf8' })
    console.log(`Created config for SSL Certificate at ${qproxyCertPath}/${commonName}.cnf\n`)
}

// Generate SSL Certificate using OpenSSL
const generateCertificate = async commonName => {
    if (fs.existsSync(`${qproxyCertPath}/${commonName}.crt`)) {
        console.log(`SSL Certificate for ${commonName} already exists at ${qproxyCertPath}/${commonName}.crt`)
        return
    }

    console.log(`Creating SSL Certificate for ${commonName}...`)

    const command = `openssl req -new -x509 -newkey rsa:2048 -sha256 -nodes -keyout "${qproxyCertPath}/${commonName}.key" -days 365 -out "${qproxyCertPath}/${commonName}.crt" -config "${qproxyCertPath}/${commonName}.cnf"`
    // console.log('command', command)

    try {
        const stdout = await execSync(command, { stdio: 'pipe' })
        // console.log(stdout)
        console.log(`SSL Certificate is created at ${qproxyCertPath}/${commonName}.crt`)
        console.log('Add Certificate to Trust Store to avoid browser raising error with Self-Signed certificate.')
        fs.unlinkSync(`${qproxyCertPath}/${commonName}.cnf`)
    } catch (err) {
        console.log('SSL Certificate creation failed.', err.message)
    }
}

// Raise register request to the Q Proxy's API Server
const register = async data => {
    try {
        const response = await fetch('http://localhost:9876', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                action: 'register',
                data
            })
        })
        const actionResponse = await response.json()
    } catch (err) {
        console.log(err.message)
        console.log('Unable to register proxy request to the running Q Proxy.\n')
        throw new Error('REGISTER_PROXY_REQUEST_FAILED')
    }
}

// Q Proxy Admin API
const api = async (req, res) => {
    if (req.method === 'GET') {
        send(res, 200, {
            method: req.method
        })
    } else if (req.method === 'POST') {
        const { action, data } = await json(req)
        // console.log('Got action request', { action, data })
        const response = await process({ action, data })
        send(res, 200, response)
    } else {
        send(res, 400, {
            method: req.method
        })
    }
}

// Q Proxy API Actions
const routeMap = {}
const process = async ({ action, data }) => {
    switch (action) {
        case 'register':
            const { port, domains } = data
            const commonName = domains[0]
            console.log('\nRegistering new route - ', data)
            console.log('---------------------------------------------------------------------------------------------\n')
            const cert = `${qproxyCertPath}/${commonName}.crt`
            const key = `${qproxyCertPath}/${commonName}.key`
            redbird.unregister(commonName)
            redbird.register(commonName, `http://localhost:${port}`, {
                ssl: {
                    port: 443,
                    redirect: true,
                    cert: cert,
                    key: key
                }
            })
            routeMap[`https://${commonName}`] = `http://localhost:${port}`
            console.log('Route Map: ', JSON.stringify(routeMap, null, 3))
            break
    }
    return {
        success: true,
        data: {}
    }
}

// Start redbird proxy
const startProxy = () => {
    redbird = new require('redbird')({
        port: 80,
        ssl: {
            port: 443,
            redirect: true,
            cert: './certs/localhost.crt',
            key: './certs/localhost.key'
        },
        bunyan: false // No logging
    })
}

const qproxy = async ({ p, d, env }) => {
    // Set global variables
    homePath = env.HOME
    qproxyConfigPath = `${homePath}/.qproxy`
    qproxyCertPath = `${homePath}/.qproxy/certs`

    // Standardize options
    console.log('Requested TLS proxying for: ')
    const port = p
    const domains = d.split(',')
    const commonName = domains[0]
    console.log('Backend Port:', port)
    console.log('Domain(s):', domains)

    // Setup - Configure config direcotry
    await setup()

    // Start the admin API if it's not running yet
    console.log('\nCheck if Q Proxy is running...')

    let isAdminServerRunning = false
    try {
        const response = await fetch('http://localhost:9876')
        const data = await response.json()
        isAdminServerRunning = true
    } catch (err) {
        isAdminServerRunning = false
    }

    // const response = fetch('http://localhost:9876/', { method: 'POST', body: JSON.stringify(body), headers: { 'Content-Type': 'application/json' } })

    if (!isAdminServerRunning) {
        console.log('Q Proxy is not running, starting now...\n')
        const adminServer = micro(api)

        try {
            await startProxy()
            adminServer.listen(9876)
            console.log('Q Proxy is started. Admin API is available on port 9876.\n')
        } catch (err) {
            console.log('Failed to start Q Proxy Admin API on port 9876.\n')
        }
    }

    // Create config file
    await generateConfig(domains)

    // Create cert & key pair
    await generateCertificate(commonName)

    // Send register request to the running proxy server
    await register({
        port,
        domains
    })
}

module.exports = { qproxy }
