Method 1: (Used this for SAN) 

Source: https://somoit.net/security/security-create-self-signed-san-certificate-openssl

* Config file

```bash
[ req ]
default_bits       = 4096
default_md         = sha512
default_keyfile    = server.key
prompt             = no
encrypt_key        = no
distinguished_name = req_distinguished_name
x509_extensions    = v3_req

# distinguished_name
[ req_distinguished_name ]
countryName            = "US"                                        # C=
stateOrProvinceName    = "California"                                # S=
localityName           = "San Francisco"                             # L=
organizationName       = "Function of Q Technology Inc."             # O=
organizationalUnitName = "Dott Product"                              # OU=
commonName             = "id.dotts.co"                               # CN=
emailAddress           = "human@earth.org"                           # CN/emailAddress=

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[ alt_names ]
DNS.0 = "id.dotts.co"
```
* Command

```bash
openssl req -new -x509 -newkey rsa:2048 -sha256 -nodes -keyout server.key -days 3560 -out server.crt -config server.cnf
```

Method 2: (Not Used) 

Source: https://devcenter.heroku.com/articles/ssl-certificate-self

# Generate private key and certificate signing request

```bash
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
openssl rsa -passin pass:x -in server.pass.key -out server.key
rm server.pass.key
openssl req -config server.cnf -new -key server.key -out server.csr
```

# Generate SSL certificate

```bash
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt
```

